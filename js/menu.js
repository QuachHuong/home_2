var menuitem={
    init: function(){
        this.clickShowMenu('#menu-item1','#click1','active1');
        this.clickShowMenu('#menu-item2','#click2','active1');
    },
    clickShowMenu: function(button,menu,classmenu){
        var btn = document.querySelector(button);
        var navmenu = document.querySelector(menu);
        btn.addEventListener('click',function(){
            navmenu.classList.toggle(classmenu);
            btn.classList.toggle('active1');
        });
    }
}
menuitem.init();
// menu bar
var openmenu = document.querySelectorAll('#open-menu');
for( var j = 0 ; j < openmenu.length; j++){
    openmenu[j].addEventListener('click',function(){
        console.log(this.nextElementSibling);
        this.nextElementSibling.classList.add('active');
    });
}
var closemenu = document.querySelectorAll('#close-menu');
for(var i=0; i<closemenu.length; i++){
    closemenu[i].addEventListener('click',function(){
        console.log(this.closest('.header_mobile-menu'));
        this.closest('.header_mobile-menu').classList.remove('active');
    });
}